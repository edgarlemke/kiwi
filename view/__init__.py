#!/usr/bin/python3
# coding: utf-8


import logging
from os.path import abspath, dirname


def _load_template(path):

    scriptdir = abspath(dirname(__file__))
    logging.debug("scriptdir: %s" % scriptdir)

    # path_ is the actual dir
    path_ = "%s/templates/%s" % (scriptdir,path)
    with open(path_,"r") as fd:
        template = fd.read()
        logging.debug("template: %s" % template)
        return template


def _render(template_,header_tags):

    bg_template = _load_template("background.html")
    template = _load_template(template_)

    bg_template = bg_template.replace("$BODY",template)
    bg_template = bg_template.replace("$HEADER_TAGS",header_tags)

    return bg_template


def render_guiche():

    header_tags = """<script type="text/javascript" src="guiche.js"></script>
    <link href="guiche.css" rel="stylesheet">"""

    return _render("guiche.html",header_tags)


def render_painel():

    header_tags = """<script type="text/javascript" src="painel.js"></script>
    <link href="painel.css" rel="stylesheet">"""

    return _render("painel.html",header_tags)

const GUICHE_TYPE = 0
const PAINEL_TYPE = 1

var socket = null

async function setup_ws () {
  socket = new WebSocket("ws://kiwi:8000/event")
  socket.onmessage = ws_onmessage

  wait_ws(hello)

  window.onbeforeunload = function () {
    socket.onclose = function(){}
    socket.close()
  }
}

function wait_ws (callback) {
  setTimeout(
    function () {
      if(socket.readyState === 1) {
        console.log("WebSocket conectado")
        callback()
      }
      else {
        console.log("Esperando por conexão WebSocket...")
        wait_ws(callback)
      }
    },
    200 // milliseconds
  )
}

function send (data) {
  socket.send(JSON.stringify(data))
}

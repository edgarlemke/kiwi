function ws_onmessage (e) {
  // console.log(e)

  var json = JSON.parse(e.data)
  console.log(json)

  switch(json.event)  {
    case 'ans_hello':
      ans_hello(json)
      break

    case 'call':
    case 'call_again':
      call(json)
      break;
    }
}


function hello () {
  send({
      event: "hello",
      type: PAINEL_TYPE,
      // label: GUICHE
  })
}

function ans_hello (json) {
  if (json.status != 0) {
    console.log(json)
    alert("Não foi possível registrar painel no servidor.")
    window.location.reload(true)
  }
  console.log("Painel registrado")
}

function call (json) {
  shift_called()
  current_passwd_div.textContent = json.passwd
  current_guiche_div.textContent = json.guiche

  blink();
  audio.play()
  setTimeout(function(){ blink(); audio.play() },3000);
}

function shift_called() {
  console.log(called_passwd_divs)
  for( var i = called_passwd_divs.length - 1; i > 0; i-- ) {
    var passwd_belowDiv = called_passwd_divs[i]
    var passwd_upperDiv = called_passwd_divs[i-1]
    passwd_belowDiv.textContent = passwd_upperDiv.textContent

    var guiche_belowDiv = called_guiche_divs[i]
    var guiche_upperDiv = called_guiche_divs[i-1]
    guiche_belowDiv.textContent = guiche_upperDiv.textContent
  }

  called_passwd_divs[0].textContent = current_passwd_div.textContent
  called_guiche_divs[0].textContent = current_guiche_div.textContent
}

function blink () {
  current_cell_div.classList.toggle("blink")
  setTimeout(function(){current_cell_div.classList.toggle("blink")},2000)
}

audio = new Audio("dumdum.wav")

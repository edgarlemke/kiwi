function setup_popup () {

  POPUP = PARAMS.get("popup")

  console.log(POPUP)
  if( POPUP == null ) {
    console.log(window.location)

    var url = window.location.href + "&popup=1"

    var width = 400
    var vpwidth = document.documentElement.clientWidth - width

    var params = "left="+vpwidth+",top=0,width="+width+",height=350,menubar=no,toolbar=no,location=no,status=no,resizable=no,scrollbars=no"
    var popup_ = window.open(url,"popup_",params)
    // popup_.focus()
    console.log(popup_)

    window.close()
  }

}


function setup_refs () {
  main_container = document.getElementById("main-container")
  guicher_number = document.getElementById("guiche-number")

  next_passwd_field = document.getElementById("next_passwd")
  attending_passwd_field = document.getElementById("attending_passwd")
  call_next_row = document.getElementById("call-next-row")
  call_again_row = document.getElementById("call-again-row")
  adjust_container = document.getElementById("adjust-container")
  adjust_button_container = document.getElementById("adjust-button-container")
  adjust_field = document.getElementById("adjust_field")
  end_row = document.getElementById("end-row")

  overlay = document.getElementById("overlay")
}


function ws_onmessage (e) {
  // console.log(e)

  var json = JSON.parse(e.data)
  console.log(json)

  var eventfunc = [
    ans_hello,
    ans_call,
    ans_call_again,
    ans_adjust,
    update_next_passwd,
    update_attending_passwd,
    end_ok
  ]
  for( var i = 0; i < eventfunc.length; i++ ) {
    func = eventfunc[i]
    if( func.name == json.event )
      func(json)
  }

}


function hello () {
  send({
      event: "hello",
      type: GUICHE_TYPE,
      guiche: GUICHE
  })
}

function ans_hello (json) {
  if (json.status != 0) {
    console.log(json)
    alert("Não foi possível registrar cliente no servidor!")
    window.location.reload(true)
  }
  console.log("Cliente registrado")
  toggle_overlay()
}


function call () {
  toggle_overlay()
  send({
    event: "call",
    guiche: GUICHE
  })
}

function ans_call (json) {
  toggle_overlay()
  update_attending_passwd(json)

  blinkfield(attending_passwd_field.parentElement.parentElement,"yellow")
}


function adjust () {
  toggle_overlay()
  send({
    event: "adjust",
    passwd: adjust_field.value
  })
}

function ans_adjust (json) {
  toggle_overlay()

  blinkfield(next_passwd_field.parentElement.parentElement,"yellow")
}


function call_again () {
  toggle_overlay()
  send({
    event: "call_again",
    guiche: GUICHE
  })
}

function ans_call_again (json) {
  toggle_overlay()

  blinkfield(attending_passwd_field.parentElement.parentElement,"yellow")
}

function end () {
  toggle_overlay()
  send({
    event: "end",
    guiche: GUICHE
  })
}


function show_container () {
  if( POPUP != null )
    main_container.classList.toggle("hidden")
}

function set_guiche_number () {
  // To avoid user mangling on GUICHE parameter
  if( isNaN(GUICHE) )
    window.close()
  guicher_number.textContent = GUICHE
}

function toggle_overlay () {
  overlay.classList.toggle("hidden")
}

function update_next_passwd (json) {
  next_passwd_field.value = json.passwd

  // if (!adjust_button_container.hidden)
  hide_adjust()
}

function update_attending_passwd (json) {
  attending_passwd_field.value = json.passwd

  show_on_call()
}

function show_adjust () {
  if(adjust_button_container.classList.contains("hidden"))
    adjust_button_container.classList.remove("hidden")
}

function hide_adjust () {
  if(!adjust_button_container.classList.contains("hidden"))
    adjust_button_container.classList.add("hidden")
}

function show_on_call () {
  call_next_row.classList.toggle("hidden")
  adjust_container.classList.toggle("hidden")
  call_again_row.classList.toggle("hidden")
  end_row.classList.toggle("hidden")
}

function hide_on_end () {
  call_next_row.classList.toggle("hidden")
  adjust_container.classList.toggle("hidden")
  call_again_row.classList.toggle("hidden")
  end_row.classList.toggle("hidden")
}

function end_ok () {
  hide_on_end()
  attending_passwd_field.value = ""

  toggle_overlay()
  blinkfield(attending_passwd_field.parentElement.parentElement,"lightgreen")
}

function blinkfield (el,color) {
  el.classList.toggle("blinkfield")
  el.style = "background-color: " + color + ";"
  setTimeout(function(){el.classList.toggle("blinkfield"); el.style=""},1000);
}

#!/usr/bin/python3
# coding: utf-8


import logging

import mysql.connector

from config import *


EVENT_ADJUST = 0
EVENT_CALL = 1
EVENT_CALL_AGAIN = 2
EVENT_END = 3


def connect():
    try:
        cnx = mysql.connector.connect(
            user= DB_USER,
            password= DB_PASSWD,
            host= DB_HOST,
            database= DB_DATABASE
        )
    except mysql.connector.Error as err:
        logging.error(err)

    return cnx


def get_next_passwd():

    query = """SELECT next_passwd FROM globals"""

    with connect() as cnx:
        with cnx.cursor() as cur:
            cur.execute(query)
            return cur.fetchone()[0]

def set_next_passwd(passwd):

    # query = """UPDATE globals SET next_passwd = %s"""
    query = """UPDATE globals SET next_passwd = %s"""

    with connect() as cnx:
        with cnx.cursor() as cur:
            cur.execute(query, (passwd,))
            cnx.commit()


def get_attending_passwd(guiche):

    query = """SELECT passwd FROM event WHERE guiche_label = %s AND event_code = %s ORDER BY datetime DESC LIMIT 0,1"""

    with connect() as cnx:
        with cnx.cursor() as cur:
            #
            cur.execute(query, (guiche, EVENT_CALL,))

            row = cur.fetchone()
            if row is None:
                logging.debug("Zero EVENT_CALL rows for guiche %s" % guiche)
                return None

            last_att_passwd = row[0]

            #
            cur.execute(query, (guiche, EVENT_END,))
            row = cur.fetchone()
            if row is None:
                logging.debug("Zero EVENT_END rows for guiche %s" % guiche)
                return last_att_passwd

            last_end_passwd = row[0]

            if last_att_passwd == last_end_passwd:
                logging.debug("last_att_passwd == last_end_passwd")
                logging.debug("last_att_passwd: %s" % last_att_passwd)
                logging.debug("last_end_passwd: %s" % last_end_passwd)
                return None

            return last_att_passwd


def _insert(guiche,event_code,passwd):

    query = """INSERT INTO event (event_code,guiche_label,passwd) VALUES(%s,%s,%s)"""

    with connect() as cnx:
        with cnx.cursor() as cur:
            # passwd = get_next_passwd()
            cur.execute(query, (event_code, guiche, passwd,))
            cnx.commit()

            # return passwd

def call(guiche,passwd):
    _insert(guiche,EVENT_CALL,passwd)

def call_again(guiche,passwd):
    _insert(guiche,EVENT_CALL_AGAIN,passwd)

def end(guiche):
    passwd = get_attending_passwd(guiche)
    _insert(guiche,EVENT_END,passwd)

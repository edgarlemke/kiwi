#!/usr/bin/python3
# coding: utf-8

import logging

import falcon.asgi

from controller.guiche import Guiche
from controller.painel import Painel
from controller.event import Event


logging.basicConfig(filename="log.txt",level=logging.DEBUG)

app = application = falcon.asgi.App()
app.add_route("/guiche", Guiche())
app.add_route("/painel", Painel())
app.add_route("/event", Event())

#!/bin/bash

profile="senhascovid"
profiledir="$HOME/.mozilla/firefox/$profile"

# Create firefox profile for our preferred settings
firefox --createprofile "$profile $profiledir"

# Start firefox with the created profile to fully setup it
firefox -P $profile &
pid=$!
#echo $pid

#sleep 30
#kill -9 $pid

# Substitute window size

#!/bin/bash

if [ $(id -u) -ne 0 ]; then
  echo "Execute este script como usuário root."
  exit
fi


echo -n "Digite usuário do proxy: "
read proxy_user

echo -n "Digite senha do proxy: "
read -s proxy_passwd
echo

export http_proxy="http://$proxy_user:$proxy_passwd@proxy.empresa.local:8080/"
export https_proxy=$http_proxy
export ftp_proxy=$http_proxy
export socks_proxy=$http_proxy
export all_proxy=$http_proxy


pip3 install "falcon>=3.*" websockets uvicorn mysql-connector-python

apt-get update
apt-get install nginx mariadb-server firefox-esr

rm /etc/nginx/sites-enabled/default
cp nginx /etc/nginx/sites-available/kiwi
ln -s /etc/nginx/sites-available/kiwi /etc/nginx/sites-enabled/kiwi

systemctl restart nginx

echo "" >> /etc/hosts
echo "127.0.0.1 kiwi" >> /etc/hosts

mysql < dump.sql
echo "CREATE USER 'kiwi'@'localhost' IDENTIFIED BY 'kiwi'; GRANT ALL PRIVILEGES ON kiwi.* TO kiwi@'localhost';" | mysql

#!/usr/bin/python3
# coding: utf-8


import falcon

from view import render_guiche


class Guiche:

    async def on_get(self, req, resp):
        template = render_guiche()

        resp.status = 200
        resp.content_type = falcon.MEDIA_HTML
        resp.data = bytes(template.encode("utf-8"))

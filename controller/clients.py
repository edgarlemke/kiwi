#!/usr/bin/python3
# coding: utf-8


import collections
import json
import logging

import model


GUICHE_TYPE = 0
PAINEL_TYPE = 1


class EventClient:

    event_func = ["hello"]

    def __init__(self,ws):
        self.ws = ws
        self.msgs = collections.deque()

    def send(self,dict_):
        msg = json.dumps(dict_)
        self.msgs.append(msg)

    @staticmethod
    def broadcast(cls,dict_):
        for eachobj in cls.objs:
            eachobj.evclient.send(dict_)

    def hello(self,jsondict):
        type = jsondict["type"]

        if type == GUICHE_TYPE:
            clientobj = GuicheClient(self)
            clientobj.hello(jsondict)
            pass

        elif type == PAINEL_TYPE:
            clientobj = PainelClient(self)
            clientobj.hello(jsondict)
            pass

        else:
            raise Exception("Invalid type")


class GuicheClient:

    objs = []
    event_func = ["adjust","call","call_again","end"]

    def __init__(self,evclient):
        self.evclient = evclient
        self.__class__.objs.append(self)

    def __del__(self):
        self.__class__.objs.remove(self)

    @classmethod
    def broadcast(cls,dict_):
        for eachobj in cls.objs:
            eachobj.evclient.send(dict_)


    def hello(self,jsondict):
        next_passwd = model.get_next_passwd()
        attending_passwd = model.get_attending_passwd( jsondict["guiche"] )

        self.evclient.send( {"event":"ans_hello","status":0} )
        self.evclient.send( {"event":"update_next_passwd","passwd":next_passwd} )

        if attending_passwd is not None:
            self.evclient.send( {"event":"update_attending_passwd","passwd":attending_passwd} )

    def adjust(self,jsondict):
        model.set_next_passwd( jsondict["passwd"] )
        next_passwd = model.get_next_passwd()

        self.evclient.send( {"event":"ans_adjust","passwd":next_passwd} )
        GuicheClient.broadcast( {"event":"update_next_passwd","passwd":next_passwd} )

    def call(self,jsondict):
        guiche = jsondict["guiche"]
        passwd = model.get_next_passwd()

        model.call(guiche,passwd)

        next_passwd = passwd+1
        model.set_next_passwd(next_passwd)

        self.evclient.send( {"event":"ans_call","passwd":passwd} )
        GuicheClient.broadcast( {"event":"update_next_passwd","passwd":next_passwd} )
        PainelClient.broadcast( {"event":"call","passwd":passwd,"guiche":guiche} )

    def call_again(self,jsondict):
        guiche = jsondict["guiche"]
        passwd = model.get_attending_passwd( guiche )

        model.call_again(guiche,passwd)

        self.evclient.send( {"event":"ans_call_again","passwd":passwd} )
        PainelClient.broadcast( {"event":"call_again","passwd":passwd,"guiche":guiche} )

    def end(self,jsondict):
        model.end( jsondict["guiche"] )

        self.evclient.send( {"event":"end_ok"} )


class PainelClient:

    objs = []
    event_func = []

    def __init__(self,evclient):
        self.evclient = evclient
        self.__class__.objs.append(self)

    def __del__(self):
        self.__class__.objs.remove(self)

    @classmethod
    def broadcast(cls,dict_):
        for eachobj in cls.objs:
            eachobj.evclient.send(dict_)


    def hello(self,jsondict):
        self.evclient.send( {"event":"ans_hello","status":0} )

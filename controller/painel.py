#!/usr/bin/python3
# coding: utf-8


import falcon

from view import render_painel


class Painel:

    async def on_get(self, req, resp):
        template = render_painel()

        resp.status = 200
        resp.content_type = falcon.MEDIA_HTML
        resp.data = bytes(template.encode("utf-8"))

#!/usr/bin/python3
# coding: utf-8


import asyncio
import json
import logging

import falcon

from .clients import EventClient,GuicheClient,PainelClient


class Event:

    async def on_websocket(self, req, ws):
        try:
            await ws.accept()
        except falcon.WebSocketDisconnected:
            return

        evclient = EventClient(ws)
        sink_task = falcon.create_task(Event.sink(evclient))

        while not sink_task.done():

            while evclient.ws.ready and not evclient.msgs and not sink_task.done():
                await asyncio.sleep(1)

            try:
                # force IndexError if deque is empty, so it doesn't get hung
                # in the while loop below
                evclient.msgs[0]

                while len(evclient.msgs):
                    msg = evclient.msgs.popleft()
                    await evclient.ws.send_text(msg)

                pass

            # TODO: break except in two, when socket disconnected remove EventClient from client list
            except (falcon.WebSocketDisconnected,IndexError) as e:
                break

            except Exception as e:
                logging.error(e)
                break

        sink_task.cancel()
        try:
            await sink_task
        except asyncio.CancelledError:
            pass

    @staticmethod
    async def sink(evclient):

        found_evclient = False
        clientobj = evclient
        while True:

            # check if our evclient is in PainelClients and GuicheClients instances
            if not found_evclient:
                objs = PainelClient.objs + GuicheClient.objs
                for eachobj in objs:
                    if eachobj.evclient is evclient:
                        evclient_ = eachobj.evclient

                        found_evclient = True
                        clientobj = eachobj

            if not found_evclient:
                evclient_ = evclient

            try:
                msg = await evclient_.ws.receive_text()
                logging.info("Msg from client: %s" % msg)
            except falcon.WebSocketDisconnected:
                break

            jsondict = json.loads(msg)

            event_func = clientobj.__class__.event_func

            event = jsondict["event"]
            found = False
            for func in event_func:
                if event == func:
                    execfuncstr = "clientobj.%s(jsondict)" % func
                    execfunc = exec(execfuncstr)
                    found = True

            if not found:
                evclient_.send("Invalid event: %s" % event)
                pass
